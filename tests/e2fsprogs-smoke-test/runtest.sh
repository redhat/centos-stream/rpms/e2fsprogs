#!/bin/bash
#
# Copyright (c) 2017 Red Hat, Inc.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Branislav Nater <bnater@redhat.com>

PACKAGE=e2fsprogs

. /usr/share/beakerlib/beakerlib.sh || exit 1

rlJournalStart

rlPhaseStartSetup "Creating few loop devices with various filesystems"
  LOOPDIR=`mktemp -d`
  FAKE=$LOOPDIR/fake-disk
  MOUNTPOINT=/tmp/testing-mountpoint
  mkdir -p $MOUNTPOINT
  rlRun "dd if=/dev/zero of=$FAKE count=50 bs=1024k" 0 "Creating $FAKE (50 MB)"
  LOOP_DEV=`losetup -f`
  rlRun "losetup $LOOP_DEV $FAKE" 0 "Assign $FAKE to $LOOP_DEV"
  DEBUGFS=debugfs
  RESIZEFS=resize2fs
  DUMPFS=dumpe2fs
  TUNEFS=tune2fs
  FSCK=e2fsck
  IMAGE=e2image
  LABEL=e2label
rlPhaseEnd

debugfs_test(){
  TARGET=$1
  BLOCKSIZE="$2"
  rlRun "$DEBUGFS $TARGET -R 'quit'"
  rlRun "$DEBUGFS $TARGET -R 'quit' -w"
  rlRun "$DEBUGFS $TARGET -R 'quit' -c"
  rlRun "$DEBUGFS $TARGET -R 'quit' -cw"
  if [ "$BLOCKSIZE" != "" ]
  then
    rlRun "$DEBUGFS $TARGET -R 'quit' $BLOCKSIZE"
    rlRun "$DEBUGFS $TARGET -R 'quit' -w $BLOCKSIZE"
    rlRun "$DEBUGFS $TARGET -R 'quit' -c $BLOCKSIZE"
    rlRun "$DEBUGFS $TARGET -R 'quit' -cw $BLOCKSIZE"
  fi
  rlRun "$DEBUGFS -V"
}

dumpe2fs_test(){
  TARGET=$1
  BLOCKSIZE="${2:3}"
  rlRun "$DUMPFS -b $TARGET"
  rlRun "$DUMPFS -f $TARGET"
  rlRun "$DUMPFS -b -f $TARGET"
  rlRun "$DUMPFS -h $TARGET"
  rlRun "$DUMPFS -x $TARGET"
  rlRun "$DUMPFS -V"
}

e2fsck_test(){
  TARGET=$1
  for op0 in "-n" "-p" "-y"
  do
    for op1 in "" "-c" "-c -c" "-f" "-f -c" "-f -c -c" "-c -k" "-c -c -k" "-f -c -k" "-f -c -c -k"
    do
      for op2 in "" "-d" "-d -D" "-D"
      do
        for op3 in "" "-t" "-t -t" "-v" "-v -t" "-v -t -t"
        do
          # -n isn't compatible with -D
          if echo "$op0 $op1 $op2 $op3" | grep -- "-n" | grep -q -- "-D"; then continue; fi
          # -n isn't compatilble with -c
          if echo "$op0 $op1 $op2 $op3" | grep -- "-n" | grep -q -- "-c"; then continue; fi

          echo "$FSCK $TARGET $op0 $op1 $op2 $op3"
          rlRun "$FSCK $TARGET $op0 $op1 $op2 $op3 > /dev/null" 0
        done
      done
    done
  done
  rlRun "$FSCK -V"
}

e2image_test(){
  TARGET=$1
  fil=`mktemp`
  f="f"

  # -f option only supports on RHEL7 and e2fsprogs-1.41.12-20.el6 (6.6 and later)
  if rlIsRHEL "<6.6"; then
    f=""
  fi

  rlRun "$IMAGE $TARGET - >/dev/null"
  rlRun "$IMAGE -r$f $TARGET - >/dev/null"

  if rlIsRHEL 5
  then
    rlRun "$IMAGE -s $TARGET - >/dev/null"
    rlRun "$IMAGE -rs $TARGET - >/dev/null"
  fi

  rlRun "$IMAGE $TARGET $fil"
  rlRun "$IMAGE -r$f $TARGET $fil"
  if rlIsRHEL 5
  then
    rlRun "$IMAGE -s $TARGET $fil"
    rlRun "$IMAGE -rs $TARGET $fil"
  fi
}

e2label_test(){
  TARGET=$1
  rlRun "$LABEL $TARGET mylabel" 0
  rlAssertEquals "Testing the label was correctly set" "`$LABEL $TARGET`" "mylabel"
}

resize2fs_test(){
  TARGET=$1
  rlRun "$RESIZEFS $TARGET"
  rlRun "$RESIZEFS $TARGET rd" 1
  rlRun "$RESIZEFS $TARGET 30M"
  if [ "$FSTYPE" != "ext4" ]; then
    # ext4 with 4k block size require min size > 20M
    rlRun "$RESIZEFS $TARGET 20M"
    rlRun "$RESIZEFS $TARGET 10M"
  fi
  rlRun "$RESIZEFS $TARGET 50M"
  rlRun "$RESIZEFS $TARGET 30M"
  rlRun "$RESIZEFS $TARGET 50M"
  for fl0 in "" "-d 1" "-F" "-d 1 -F"
  do
    for fl1 in "" "-p" "-f" "-p -f"
    do
      rlRun "$RESIZEFS $TARGET 30M $fl0 $fl1"
      rlRun "$RESIZEFS $TARGET 50M $fl0 $fl1"
    done
  done
}

tune2fs_test(){
  TARGET=$1
  MNT=$2
  rlRun "$TUNEFS -c 100 $TARGET"
  rlRun "$TUNEFS -C 100 $TARGET"
  rlRun "$TUNEFS -c bogus $TARGET" 1
  rlRun "$TUNEFS -C bogus $TARGET" 1

  for t in d m w
  do
    rlRun "$TUNEFS -i 2$t $TARGET" 0
    if rlIsRHEL 5
    then
      rlRun "$TUNEFS -i 9999999999$t $TARGET" 0
    fi
    rlRun "$TUNEFS -i 0$t $TARGET" 0
  done
  rlRun "$TUNEFS -i bogus $TARGET" 1
  rlRun "$TUNEFS -l $TARGET" 0
  rlRun "$TUNEFS -L tune2 $TARGET" 0
  rlAssertEquals "Checking if label was set correctly" `$LABEL $TARGET` "tune2"
  rlRun "$TUNEFS -m 10 $TARGET" 0
  rlRun "$TUNEFS -m 5 $TARGET" 0
  rlRun "$TUNEFS -M /root $TARGET" 0
  rlRun "$TUNEFS -M $MNT $TARGET" 0
  rlRun "$TUNEFS -r 1000 $TARGET" 0
  rlRun "$TUNEFS -T now $TARGET" 0
  rlRun "$TUNEFS -T 20081212 $TARGET" 0
  rlRun "$TUNEFS -T 20081212121212 $TARGET" 0
  rlRun "$TUNEFS -u root $TARGET" 0
  umount $TARGET
  # It refuses to run '$TUNEFS -U clear $TARGET' without do fsck first.
  $FSCK -fy $TARGET > /dev/null
  original="`$TUNEFS -l $TARGET | grep UUID`"
  rlRun "$TUNEFS -U clear $TARGET" 0
  rlAssertNotEquals "Check the UUID was cleared" "$original" "`$TUNEFS -l $TARGET | grep UUID`"
  original="`$TUNEFS -l $TARGET | grep UUID`"
  rlRun "$TUNEFS -U random $TARGET" 0
  rlAssertNotEquals "Check the UUID was randomed" "$original" "`$TUNEFS -l $TARGET | grep UUID`"
  original="`$TUNEFS -l $TARGET | grep UUID`"
  rlRun "$TUNEFS -U time $TARGET" 0
  rlAssertNotEquals "Check the UUID was changed to time-based" "$original" "`$TUNEFS -l $TARGET | grep UUID`"
  if [ "$FSTYPE" == "ext4" ]; then
    mount $TARGET $MNT
  fi
}

mke2fs_test(){
  # https://bugzilla.redhat.com/show_bug.cgi?id=1889464#c15
  rlRun "! MKE2FS_CONFIG= mkfs.$fs 2>&1 | grep 'Syntax error'" 0
}

for fs in ext2 ext3 ext4; do
  export FSTYPE=$fs
  if rlIsRHEL 5 && [ "$fs" == "ext4" ]; then
    export DEBUGFS=debuge4fs
    export RESIZEFS=resize4fs
    export DUMPFS=dumpe4fs
    export TUNEFS=tune4fs
    export FSCK=e4fsck
    export IMAGE=e4image
    export LABEL=e4label
  fi
  for additional in "" "-b 1024" "-b 2048" "-b 4096"
  do
    rlPhaseStartTest "Testing on $fs with blocksize $additional"
      rlRun "mkfs -F -t $fs $additional $LOOP_DEV" 0
      mount $LOOP_DEV $MOUNTPOINT
      debugfs_test $LOOP_DEV "$additional"
      dumpe2fs_test $LOOP_DEV "$additional"
      e2image_test $LOOP_DEV
      e2label_test $LOOP_DEV
      tune2fs_test $LOOP_DEV $MOUNTPOINT
      umount $MOUNTPOINT; sleep 10
      rlRun "$FSCK -f -y $LOOP_DEV" 0,1,2
      resize2fs_test $LOOP_DEV
      e2fsck_test $LOOP_DEV
      mke2fs_test
    rlPhaseEnd
  done
done


rlPhaseStartCleanup
  losetup -d $LOOP_DEV
  rm -rf $FAKE $MOUNTPOINT
  rlAssertNotExists $FAKE
  rlAssertNotExists $MOUNTPOINT
#  rlCreateLogFromJournal > $OUTPUTFILE
rlPhaseEnd

rlJournalPrintText
rlJournalEnd
